# Code Challenge

Senior Software Engineer Code Challenge - 2021

## Overview

When you run the project, you should see a webpage that displays cards with information about Star Wars characters. This data is served from the [Star Wars API](https://swapi.dev/).

What we would like to add to this is the ability to "favourite" certain characters, which will then be displayed on a Favourites page.

Depending on whether you choose to attempt the front- or back-end aspect of the project will change what we need you to do for this coding exercise.

## Getting started

While this repo contains both the back and frontend of a project, you are free to choose which side you want to work on.
The backend is written in Node.js, while the frontend uses React.

You are, of course, welcome to attempt both, but we don't expect this. We'd rather you pick the aspect you're most comfortable with and give it your best shot.

## Installation

Clone repository:

```sh
git clone https://gitlab.com/apolitical/code-tests/frontend-code-challenge.git
```

And then:

```sh
cd frontend-code-challenge
```

Once the project is cloned, you will see two folders: `frontend` and `backend`, along with the `docker-compose.yml` file to spin up all the services together.

### Requirements

Before jumping into the subtasks, make use you have all the tools installed on your local machine.

- [node.js][node] 12.20.1+
- [yarn][yarn]
- [docker][docker]
- [docker-compose][docker-compose]

[node]: https://nodejs.org/en/download/
[yarn]: https://classic.yarnpkg.com/en/docs/install
[docker]: https://docs.docker.com/install/
[docker-compose]: https://docs.docker.com/compose/install/

### Environment variables

Create a copy of `default.env` file:

```sh
cp default.env .env
```

Customise if needed.

### Usage

For the frontend exercise, it's recommended to run the front and backend separately:

```
cd frontend
yarn install
yarn run start
```

This will run the frontend on [http://localhost:5000](http://localhost:5000/)

```
cd backend
cp default.env .env
yarn install
docker-compose up
```
This will run the backend on [http://localhost:3000](http://localhost:3000/).

You will need to have both running in order to properly access the functionality.


----

Alternatively, you can compose the containers to get both the `frontend` and `backend` services running:

```sh
docker-compose up
```

You should now be able to access:
- The frontend on [http://localhost:5000](http://localhost:5000/)
- The backend on [http://localhost:3000](http://localhost:3000/)

In order to stop the services:

```sh
docker-compose down
```

If you need to run each project separately, please, follow the instructions on the `README` file on the `frontend` or `backend` folder.

### Frontend Subtasks

We would like you to implement:

- Pagination:
  - Description:
    We want to display a max of 10 cards per page, and have a way that users can navigate between pages to see further cards.

    In `Home.jsx`, we make an API call that fetches 10 characters. This uses a state object called `currentPage` to determine what page we fetch from the API. The API response returns the total count of characters, the link for the next page of the API and an array of 'character' objects that we use to populate the cards.
    This file also uses a component called `Pagination.jsx`.
    For this task, we'd like you to work on the pagination so that:
    - There is a button displayed for every page (10 character cards per page)
    - Clicking on a button updates `currentPage` and displays the character cards for that page

- Favouriting:
  - Description:
  We want to allow users to "favourite" certain characters.
  By default, all cards in the "view all" page appear to be selected, but this does not represent the user's actual favourites.
  For this task, we would like you to use this data to mark whether a card has been "favourited" or not, based on data we have about the user.

- Styling:
  - Description:
    On desktop, the site looks fine. However, on a mobile device, it's quite poor. We'd like you to use your own best judgement to add styling to this view to make it look better to a user. The changes you make for mobile should not negatively impact the desktop view.




## Constraints

If we are working on this as a pairing exercise, we'd like you to implement as much as you can in the time that we have. It's most important to us that we can see your thought process and how you approach the problem. Please feel free to Google syntax, add console logs where helpful, commit as you see fit and narrate your thought process as you go.


We give you this repository in advance so you can get the project set up. Please do feel free to read through them and have a look at the code to familiarise yourself if you want to, but we would prefer that you do not work on the tasks outside of the pairing session. We want to be mindful of your time, but it's also easier for us to see your approach if we do it together.



### Frontend

You’re welcome to use external libraries/packages necessary to build/run your project (ie. things like React, Vue, StyledComponents, Babel etc), but we would prefer that the logical requirements are done yourself, such as fetching the data, cleaning it up etc. We don’t expect to see production ready code, but we do want to see how you tackle the problem. Making clearly named commits and commenting your code as you go is encouraged.

For styling, please feel free to use what you’re most comfortable with. We work with Styled Components, but we’re happy to see submissions in vanilla CSS, Sass/Less or similar if you'd rather add those. Our one ask here is that you do not use design component libraries such as Material Design or Base Web, as we’d like to see your skills.
Feel free to be as creative as you like with the UI!


## Submission

Please, in order to submit your work, create a new branch on the repository with your name so that we can review and see the commit history for your changes. Make sure you follow the frequent commit practice so that your branch indicates reasonable milestones of your implementation. Good luck!
