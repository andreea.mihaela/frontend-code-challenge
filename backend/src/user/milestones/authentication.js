'use strict';

const {
  Errors: { BadRequestError },
} = require('finale-rest');

const {
  API: {
    HEADERS: { X_SLUG },
    SLUGS: { MYSELF },
  },
} = require('../../config');

module.exports = (req, res, context) => {
  if (req.params.slug === MYSELF) {
    const encodedSlug = req.header(X_SLUG);
    if (!encodedSlug) {
      throw new BadRequestError('Missing user slug', ['header', X_SLUG]);
    }
    req.params.slug = Buffer.from(encodedSlug, 'base64').toString('ascii');
  }
  return context.continue;
};
